
 A completing-read front-end for browsing and acting on bibliographic data.

 When used with vertico/selectrum/icomplete-vertical, embark, and marginalia,
 it provides similar functionality to helm-bibtex and ivy-bibtex: quick
 filtering and selecting of bibliographic entries from the minibuffer, and
 the option to run different commands against them.
