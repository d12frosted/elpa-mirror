This library creates an explorable box diagram for a given list.
This is helpful for learning about linked-list structures in Elisp.
