This library provides functions for posting text from Emacs to the
750words.com website.

See https://github.com/zzamboni/750words-client for full usage instructions.
