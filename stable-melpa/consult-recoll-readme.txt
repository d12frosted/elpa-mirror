A `consult-recoll' command to perform simple interactive queries
over your Recoll (https://www.lesbonscomptes.com/recoll/) index.
See the corresponding customization group for ways to tweak its
behaviour to your needs.
