`Lister` is a library for creating interactive "lists" of any kind.
In contrast to similar packages like `hierarchy.el` or
`tablist.el`, it aims at *not* simply mapping a data structure to a
navigatable list.  Rather, it treats the list like Emacs treats
buffers: A list is an empty space to which you can successively add
stuff.  So in Emacs lingo, `lister` should be rather called
`listed` - it is a library for *editing* lists, instead of
displaying them.

For more information, read the README.org shipped with that package.
