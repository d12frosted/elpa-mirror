This package provides FAR-like file manager.

To start eFar just type M-x efar.
When Efar is called with universal argument (C-u M-x efar),
default-directory of actual buffer is automatically opened in left panel.

Press C-? to show buffer with all available key bindings.

Use M-x customize to configure numerous eFar parameters
including key bindings and faces.
