Approximate string matching completion style with a scoring
algorithm that factors in substring matches and word/path
component/camelCase boundaries.

See: Myers, Eugene W., and Webb Miller. "Optimal alignments in
     linear space." Bioinformatics 4.1 (1988): 11-17.
