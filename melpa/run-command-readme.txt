
Leave Emacs less.  Relocate those frequent shell commands to configurable,
dynamic, context-sensitive lists, and run them at a fraction of the
keystrokes with autocompletion.
