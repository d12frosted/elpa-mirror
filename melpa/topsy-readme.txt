This library shows a sticky header at the top of the window.  The
header shows which definition the top line of the window is within.
Intended as a simple alternative to `semantic-stickyfunc-mode`.

Mode-specific functions may be added to `topsy-mode-functions'.

NOTE: For Org mode buffers, please use org-sticky-header:
<https://github.com/alphapapa/org-sticky-header>.
