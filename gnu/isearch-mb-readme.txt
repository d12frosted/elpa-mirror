This package provides an alternative isearch UI based on the
minibuffer.  This allows editing the search string in arbitrary
ways without any special maneuver; unlike standard isearch, cursor
motion commands do not end the search.  Moreover, the search status
information in the echo area and some keybindings are slightly
simplified.

To use the package, simply activate `isearch-mb-mode'.